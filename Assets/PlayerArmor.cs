﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArmor : MonoBehaviour
{
    private GUIText armor;
    public static int armorCurrent, armorMax;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        armor = GetComponent<GUIText>();
    }

    // Update is called once per frame
    void Update()
    {
        armor.text = armorCurrent.ToString() + "/" + armorMax.ToString();
        
        if (armorCurrent>25)
        {
            armor.color=Color.white;
        }
        else if (armorCurrent < 25)
        {
            lowArmor();
        }
    }

    private void lowArmor()
    {
        timer = timer + Time.deltaTime;
        if ((int)timer % 2 == 0)
        {
            armor.color = Color.red;
        }
        else
        {
            armor.color = Color.yellow;

        }
    }
}
