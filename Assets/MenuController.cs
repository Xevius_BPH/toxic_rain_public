﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    private GameObject title,options,contTit,contMain,contMenu,rainTit,rainMain,rainMenu;
    private int stage;

    // Start is called before the first frame update
    void Start()
    {

        title = GameObject.Find("Title");
        options = GameObject.Find("Options");
        contTit = GameObject.Find("Controls_Title");
        contMain = GameObject.Find("Controls_Main");
        contMenu = GameObject.Find("Controls_Menu");
        rainTit = GameObject.Find("Rain_Title");
        rainMain = GameObject.Find("Rain_Main");
        rainMenu = GameObject.Find("Rain_Menu");

        TitleScreen();


    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Start") && stage == 0)
        {
            SceneManager.LoadScene("Final", LoadSceneMode.Single);
        }

        if (Input.GetButtonDown("Help") && stage==0)
        {
            ControlsScreen();
        }

       if(Input.GetAxisRaw("Horizontal")>0 && stage==1)
        {
            RainScreen();
        }

        if (Input.GetAxisRaw("Horizontal") < 0 && stage == 1)
        {
            TitleScreen();
        }

        if (Input.GetAxisRaw("Horizontal") < 0 && stage == 2)
        {
            ControlsScreen();
        }

    }

    private void TitleScreen()
    {
        title.SetActive(true);
        options.SetActive(true);
        contTit.SetActive(false);
        contMain.SetActive(false);
        contMenu.SetActive(false);
        rainTit.SetActive(false);
        rainMain.SetActive(false);
        rainMenu.SetActive(false);
        stage = 0;

    }

    private void ControlsScreen()
    {
        title.SetActive(false);
        options.SetActive(false);
        contTit.SetActive(true);
        contMain.SetActive(true);
        contMenu.SetActive(true);
        rainTit.SetActive(false);
        rainMain.SetActive(false);
        rainMenu.SetActive(false);
        stage = 1;
    }

    private void RainScreen()
    {
        title.SetActive(false);
        options.SetActive(false);
        contTit.SetActive(false);
        contMain.SetActive(false);
        contMenu.SetActive(false);
        rainTit.SetActive(true);
        rainMain.SetActive(true);
        rainMenu.SetActive(true);
        stage = 2;
    }
}
