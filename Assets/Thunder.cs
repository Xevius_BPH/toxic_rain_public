﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunder : MonoBehaviour
{
    private Light thundLight;
    private float time,randomTime;
    public AudioClip thunder;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {

        player = GameObject.Find("Player");
        time = 10;

        thundLight = GetComponent<Light>();

        thundLight.intensity = 0.5f;

        Invoke("ThunderLight", 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        randomTime = Random.Range(30, 60);
    }

    private void ThunderLight()
    {
      thundLight.intensity = 3;
      Invoke("ThunderLightOff", 0.1f);
      AudioSource.PlayClipAtPoint(thunder, player.transform.position);
      Invoke("ThunderLight", randomTime);

    }

    private void ThunderLightOff()
    {
        thundLight.intensity = 0.5f;


    }
}
