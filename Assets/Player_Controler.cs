﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Controler : MonoBehaviour
{
    private Vector3 movRight, movLeft, runRight, runLeft;
    private Rigidbody2D rigBod;
    private float jumping,dashRight,dashLeft,coolDownDash,coolDownGun;
    private bool dashCD,onRain,underPlatform, touchGround,dead;
    public bool stopCamera, stopCameraRight,lookingRight, hasPlatformGun;
    private float CDf, timeRain;
    private int armor,health;
    public int maxPlatforms,maxLasers;
    public GameObject platform, laser, enemy,enemyShield,bones,infoBG,infoTxt;
    private PlatformController platformCon;
    private LaserController laserCon;
    private EnemyController enemyCon;
    private Animator anims;
    public AudioClip laserSound,platformSound,item;
    // Start is called before the first frame update
    void Start()
    {
        dead = false;
        infoBG.gameObject.SetActive(false);
        infoTxt.gameObject.SetActive(false);
        movRight = new Vector3(1.0f, 0, 0);
        movLeft = new Vector3(-1.0f, 0, 0);
        runRight = new Vector3(2.0f, 0, 0);
        runLeft = new Vector3(-2.0f, 0, 0);
        enemy = GameObject.Find("Enemy");
        enemyCon = enemy.GetComponent<EnemyController>();
        jumping = 4f;
        dashRight = 2.5f;
        dashLeft = -2.5f;
        rigBod = GetComponent<Rigidbody2D>();
        platformCon = platform.GetComponent<PlatformController>();
        laserCon = laser.GetComponent<LaserController>();     
        touchGround = true;
        lookingRight = true;
        dashCD = true;
        onRain = false;
        underPlatform = false;
        armor = 100;
        health = 100;
        PlayerArmor.armorMax = armor;
        PlayerHealth.healthMax = health;
        stopCamera = false;
        stopCameraRight = false;
        anims = GetComponent<Animator>();
        hasPlatformGun = false;
        coolDownGun = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Movevent();
        maxPlatforms = 3;
        maxLasers = 50;
        coolDownDash = coolDownDash - Time.deltaTime;
        coolDownGun = coolDownGun - Time.deltaTime;
        CDf = coolDownDash;
        PlayerArmor.armorCurrent = armor;
        PlayerHealth.healthCurrent = health;
        touchGround = false;



        if (health<=0)
        {
            Die();
        }

        if (CDf<=0)
        {
            coolD.CDf = 0;
        }
        else
        {
        coolD.CDf = CDf;

        }

        if (coolDownDash<=0)
        {
            dashCD = true;
        }
    }

    private void Movevent()
    {

        if (!dead)
        {
            if (Input.GetButtonDown("Jump") && touchGround)
            {
                rigBod.velocity = new Vector2(0, jumping);
                anims.Play("Run");

            }

            if (Input.GetButtonDown("ShootPlatform") && Input.GetAxisRaw("Horizontal") == 0 && hasPlatformGun)
            {
                if (coolDownGun <= 0)
                {
                    CreatePlatform();
                    coolDownGun = 0.5f;
                }
            }

            if (Input.GetButtonDown("ShootGun") && Input.GetAxisRaw("Horizontal") == 0)
            {
                if (coolDownGun <= 0)
                {
                    ShootWeapon();
                    coolDownGun = 0.5f;
                }

            }

            if (Input.GetButtonDown("Dash") && dashCD)
            {
                if (lookingRight)
                {
                    rigBod.velocity = new Vector2(dashRight, 0);
                }
                else if (lookingRight == false)
                {
                    rigBod.velocity = new Vector2(dashLeft, 0);
                }
                anims.Play("Run");
                dashCD = false;
                coolDownDash = 5;
                CDf = coolDownDash;
            }


            if (Input.GetAxisRaw("Horizontal") == 0)
            {
                anims.Play("Idle");

            }

            if (Input.GetAxisRaw("Horizontal") != 0)
            {

                if (Input.GetAxisRaw("Horizontal") > 0)
                {
                    lookingRight = true;
                    //transform.eulerAngles = new Vector3(0, 0, 0);
                    bones.transform.localScale = new Vector3(1, 1, 1);


                    if (Input.GetButton("Run"))
                    {
                        transform.Translate(runRight * Time.deltaTime, Space.World);
                        anims.Play("Run");
                    }
                    else
                    {
                        transform.Translate(movRight * Time.deltaTime, Space.World);
                        anims.Play("Walk");
                    }

                }

                if (Input.GetAxisRaw("Horizontal") < 0)
                {
                    lookingRight = false;
                    //transform.eulerAngles = new Vector3(0, 180, 0);
                    bones.transform.localScale = new Vector3(-1, 1, 1);


                    if (Input.GetButton("Run"))
                    {
                        transform.Translate(runLeft * Time.deltaTime, Space.World);
                        anims.Play("Run");
                    }
                    else
                    {
                        transform.Translate(movLeft * Time.deltaTime, Space.World);
                        anims.Play("Walk");
                    }

                }
            }
        }
    }

    
    private void OnCollisionEnter2D(Collision2D col)
    {
        //Controls the Damage Recibed by the small enemy laser
        if (col.gameObject.tag == "LaserEnemy")
        {
            if(armor>15)
            {
                armor = armor - 15;

            }
            else if (armor>0 && armor<15)
            {
                armor = 0;
            }
            else if (armor<=0 && health>25)
            {
                health = health - 25;
            }
            else if (armor <= 0 && health<25)
            {
                health = 0;
            }
        }

        //Controls the Damage Recibed by the big enemy laser
        if (col.gameObject.tag == "LaserEnemyBig")
        {
            if (armor > 15)
            {
                armor = armor - 25;

            }
            else if (armor > 0 && armor < 25)
            {
                armor = 0;
            }
            else if (armor <= 0 && health > 50)
            {
                health = health - 50;
            }
            else if (armor <= 0 && health < 50)
            {
                health = 0;
            }
            
            
                
            
        }

    }

    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground" || (col.gameObject.tag == "Platform"))
        {
            touchGround = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "HurtRain")
        {
            InvokeRepeating("RainHurt", 1, 1);
        }
        
        if (col.gameObject.tag == "ArmourRepair")
        {
            InvokeRepeating("ArmourRepair", 1, 1);

        }

        if (col.gameObject.tag == "CameraStop")
        {
            stopCamera = true;
        }

        if (col.gameObject.tag == "CameraStopRight")
        {
            stopCameraRight = true;
        }

        if (col.gameObject.tag == "TriggerEnemy")
        {
            enemyCon.WakeUp();

            enemyShield.SetActive(false);

        }

        if (col.gameObject.tag == "PowerUp")
        {
            hasPlatformGun = true;
            AudioSource.PlayClipAtPoint(item, transform.position);
            Destroy(col.gameObject);
            infoBG.gameObject.SetActive(true);
            infoTxt.gameObject.SetActive(true);
            Invoke("UiOff", 15);
        }

        if (col.gameObject.tag == "EndOfLevel" && hasPlatformGun)
        {
        SceneManager.LoadScene("Win", LoadSceneMode.Single);
        }
    }

    private void UiOff()
    {
        infoBG.gameObject.SetActive(false);
        infoTxt.gameObject.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "HurtRain")
        {
            CancelInvoke("RainHurt");
        }

        if (col.gameObject.tag == "ArmourRepair")
        {
            CancelInvoke("ArmourRepair");
        }

        if (col.gameObject.tag == "UnderPlatform")
        {
            underPlatform = false;
        }

        if (col.gameObject.tag == "CameraStop")
        {
            stopCamera = false;
        }

        if (col.gameObject.tag == "CameraStopRight")
        {
            stopCameraRight = false;
        }
    }

    private void Die()
    {
        anims.Play("PlayerDeath");
        dead = true;
        SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
    }
   

    private void CreatePlatform()
    {
        if (maxPlatforms>0)
        {
            if (lookingRight)
            {
                Instantiate(platform, new Vector3(transform.position.x + 0.2f, transform.position.y +0.1f, transform.position.z), transform.rotation);
            }
            else if (!lookingRight)
            {
                Instantiate(platform, new Vector3(transform.position.x - 0.7f, transform.position.y +0.1f, transform.position.z), transform.rotation);
            }
            anims.Play("Pium");
            AudioSource.PlayClipAtPoint(platformSound, transform.position);
        }
    }

    private void ShootWeapon()
    {
        if (maxLasers>0)
        {
            if (lookingRight)
            {
                Instantiate(laser, new Vector3(transform.position.x + 0.2f, transform.position.y + 0.1f, transform.position.z), transform.rotation);
            }
            else if (!lookingRight)
            {
                Instantiate(laser, new Vector3(transform.position.x - 0.7f, transform.position.y + 0.1f, transform.position.z), transform.rotation);
            }
            anims.Play("Pium");
            AudioSource.PlayClipAtPoint(laserSound, transform.position);

        }
    }


    private void RainHurt()
    {
        if (Input.GetAxisRaw("Horizontal") == 0)
        {              
            if (armor > 0)
            {
                armor = armor - 5;
            }
            else if (armor <= 0)
            {
                health = health - 50;
            }
        }
        else if (Input.GetAxisRaw("Horizontal") != 0)
        {
            if (!Input.GetButton("Run"))
            {
                if (armor > 0)
                {
                    armor = armor - 2;
                }
                else if (armor <= 0)
                {
                    health = health - 50;
                }
            }
        }





    }

    private void ArmourRepair()
    {

        if (armor < 100)
        {
            armor = armor + 5;
        }
       

    }
}
