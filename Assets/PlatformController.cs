﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    private float timer;
    private GameObject player;
    private Player_Controler playerScript;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerScript = player.GetComponent<Player_Controler>();
    }

    // Update is called once per frame
    void Update()
    {
        playerScript.maxPlatforms = playerScript.maxPlatforms - 1;

        DestroyPlat();
    }

    private void DestroyPlat()
    {
        timer = timer + Time.deltaTime;

        if((int)timer==5)
        {
            timer = 0;
            Destroy(gameObject);
        }
    }
}
