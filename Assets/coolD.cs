﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coolD : MonoBehaviour
{
    private GUIText debugCD;
    public static float CDf;
    private int CDfInt;
    // Start is called before the first frame update
    void Start()
    {
        debugCD = GetComponent<GUIText>();
    }

    // Update is called once per frame
    void Update()
    {
        CDfInt = (int)CDf;
        debugCD.text = CDfInt.ToString()+ " SEC";
    }
}
