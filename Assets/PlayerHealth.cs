﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    private GUIText health;
    public static int healthCurrent, healthMax;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<GUIText>();
    }

    // Update is called once per frame
    void Update()
    {
        health.text = healthCurrent.ToString() + "/" + healthMax.ToString();

        if (healthCurrent > 25)
        {
            health.color = Color.white;
        }
        else if (healthCurrent < 25)
        {
            lowArmor();
        }
    }

    private void lowArmor()
    {
        timer = timer + Time.deltaTime;
        if ((int)timer % 2 == 0)
        {
            health.color = Color.red;
        }
        else
        {
            health.color = Color.yellow;

        }
    }
}
