﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public bool lookingRight;
    private bool dead,atacking;
    public GameObject laser,laserBig,guiBg,guiLabel,powerUp,player,bones;
    private float timer;
    public int shoots,health;
    public GUIText guiHealth;
    private Animator anims;
    public AudioClip laserSoundSmall, laserSoundBig;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        dead = false;
        atacking = false;
        anims = GetComponent<Animator>();
        lookingRight = true;
        shoots = 0;
        health = 100;
        guiBg = GameObject.Find("Enemy_Background");
        guiLabel = GameObject.Find("Enemy_Label");
        guiBg.gameObject.SetActive(false);
        guiLabel.gameObject.SetActive(false);
        guiHealth.gameObject.SetActive(false);
        guiHealth.text = health.ToString() + "/100";    
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0 && !dead)
        {
            die();
        }

        if(player.transform.position.x> bones.transform.position.x && !lookingRight && !dead)
        {
            Invoke("TurnRight", 2);

        }

        if (player.transform.position.x < bones.transform.position.x && lookingRight && !dead)
        {
          
          Invoke("TurnLeft", 2);
            
           
        }


    }

    public void WakeUp()
    {
        Invoke("Atack", 1);
        guiBg.gameObject.SetActive(true);
        guiLabel.gameObject.SetActive(true);
        guiHealth.gameObject.SetActive(true);

      
    }

    private void Atack()
    {
        if (!dead)
        {
            InvokeRepeating("ShootWeapon", 0, 1);

        }
    }

    private void TurnRight()
    {
        bones.transform.localScale = new Vector3(1, 1, 1);
        lookingRight = true;
    }

    private void TurnLeft()
    {
        bones.transform.localScale = new Vector3(-1, 1, 1);
        lookingRight = false;
    }


    private void die()
    {
        dead = true;
        CancelInvoke("ShootWeapon");
        guiBg.gameObject.SetActive(false);
        guiLabel.gameObject.SetActive(false);
        guiHealth.gameObject.SetActive(false);
        anims.Play("DIE");
        Instantiate(powerUp, new Vector3(transform.position.x + 0.2f, transform.position.y + 0.0f, transform.position.z), transform.rotation);

    }

    private void ShootWeapon()
    {
       
            if (lookingRight)
            {
            Instantiate(laser, new Vector3(transform.position.x + 0.2f, transform.position.y + 0.0f, transform.position.z), transform.rotation);
            shoots++;
            }
            else if (!lookingRight)
            {
                Instantiate(laser, new Vector3(transform.position.x - 0.7f, transform.position.y + 0.0f, transform.position.z), transform.rotation);


            shoots++;
            }
            anims.Play("ShootSmall");
            AudioSource.PlayClipAtPoint(laserSoundSmall, transform.position);




        if (shoots==3)
            {
            CancelInvoke("ShootWeapon");
            Invoke("ShootWeaponBig", 1);
            Invoke("Atack", 2);
            shoots = 0;

            }
    }



    private void ShootWeaponBig()
    {
       
            if (lookingRight)
            {
                Instantiate(laserBig, new Vector3(transform.position.x + 0.2f, transform.position.y + 0.0f, transform.position.z), transform.rotation);
            }
            else if (!lookingRight)
            {
                Instantiate(laserBig, new Vector3(transform.position.x - 0.7f, transform.position.y + 0.0f, transform.position.z), transform.rotation);
            }
            anims.Play("ShootBig");
            AudioSource.PlayClipAtPoint(laserSoundBig, transform.position);




    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "LaserFriendly")
        {
            if (health>0)
            {
                health = health - 10;
                guiHealth.text = health.ToString() + "/100";
            }
            else if (health<=0)
            {
                guiHealth.text = "0/100";

            }
        }
    }
}
