﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Player_Controler playerScript;
    public Vector3 playerPosition;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        playerScript = player.GetComponent<Player_Controler>();
        transform.position = new Vector3(0.6f, 1.5f, -10);

    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {

        if (playerScript.stopCamera == true)
        {
            playerPosition = new Vector3(0.6f, 1.5f, -10);
            transform.position = Vector3.SmoothDamp(transform.position, playerPosition, ref velocity, smoothTime);
        }

        if (playerScript.stopCameraRight == true)
        {
            playerPosition = new Vector3(12.8f, 1.5f, -10);
            transform.position = Vector3.SmoothDamp(transform.position, playerPosition, ref velocity, smoothTime);
        }

        else
        {
            playerPosition = new Vector3(player.transform.position.x, 1.5f, -10);
            transform.position = Vector3.SmoothDamp(transform.position, playerPosition, ref velocity, smoothTime);
        }

    }
}
