﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    private float timer;
    private GameObject player;
    private Player_Controler playerScript;
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        playerScript = player.GetComponent<Player_Controler>();

        if (playerScript.lookingRight)
        {
            direction = new Vector3(5, 0, 0);
        }
        if (!playerScript.lookingRight)
        {
            direction = new Vector3(-5, 0, 0);

        }
    }

    // Update is called once per frame
    void Update()
    {
        playerScript.maxLasers = playerScript.maxLasers - 1;

        DestroyPlat();

        transform.Translate(direction * Time.deltaTime, Space.World);


    }

    private void DestroyPlat()
    {
        timer = timer + Time.deltaTime;

        if ((int)timer == 5)
        {
            timer = 0;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Walls")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "EnemyShield")
        {
            Destroy(gameObject);
        }
    }
}
