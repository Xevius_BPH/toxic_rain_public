﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserControllerEnemy : MonoBehaviour
{
    private float timer;
    private GameObject enemy;
    private EnemyController enemyScript;
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GameObject.Find("Enemy");
        enemyScript = enemy.GetComponent<EnemyController>();


        if (enemyScript.lookingRight)
        {
            direction = new Vector3(5, 0, 0);
        }
        if (!enemyScript.lookingRight)
        {
            direction = new Vector3(-5, 0, 0);

        }

        ;
    
        
    }

    // Update is called once per frame
    void Update()
    {
        

        transform.Translate(direction * Time.deltaTime, Space.World);


    }

    private void DestroyPlat()
    {
        timer = timer + Time.deltaTime;

        if ((int)timer == 5)
        {
            timer = 0;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Walls")
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}

